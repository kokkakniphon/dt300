from graphic import Graphic
from scene import Home_Scene

import os
import sys
import pygame

class Engine:
    def __init__(self, x, y):
        self._isRunning = True
        self.WINDOW_WIDTH = 900
        self.WINDOW_HEIGHT = 600
        # x = 2640
        # y = 40
        # os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (x,y)
        self._window = pygame.display.set_mode((self.WINDOW_WIDTH, self.WINDOW_HEIGHT))
        pygame.font.init()
        pygame.display.set_caption("DT300 - DFS, Djikstra, Kruskal *Niphon*")
        # pygame.display.set_icon(pygame.image.load("resources/Icons/Logo.png").convert_alpha())
        pygame.init()

        self._scenes = []
        self._scenes_start = []
        self._deltaTime = 0.0
        self._lastTime = 0.0

        self.focusScene = 0

        self.event = pygame.event.get()
    
    def Start(self):
        for scene in self._scenes_start:
            scene.Start()
        self._scenes_start.clear()
    def Update(self):
        for scene in self._scenes:
            scene.Update()
    def Render(self):
        self._window.fill((255,255,255))
        for scene in self._scenes:
            scene.Render()
        pygame.display.update()

    def Input(self):
        self.event = pygame.event.get()
        for event in self.event:
            if event.type == pygame.QUIT:
                self._isRunning = False
                self.Quit()

    def UpdateTime(self):
        pass
    def Run(self):
        while self._isRunning:
            t = pygame.time.get_ticks()
            self._deltaTime = (t - self._lastTime) / 1000.0
            self._lastTime = t
            self.Start()
            self.Update()
            self.Render()
            self.Input()
                
    def Quit(self):
        pygame.quit()
    def AddScene(self, scene):
        self._scenes_start.append(scene)
        self._scenes.append(scene)
        pass
    def SetSceneActive(self, index, active):
        self._scenes[index].active = active
        if active:
            self._scenes[index].OnEnable()
        else:
            self._scenes[index].OnDisable()
    def DeltaTime(self):
        return self._deltaTime
        pass
    def Time(self):
        return pygame.time.get_ticks() / 1000.0
        pass
        

def main():
    # args = sys.argv[1:]
    # x = int(args[0])
    # y = int(args[1])
    engine = Engine(0, 0)
    # engine = Engine(x, y)
    graphic = Graphic()
    engine.AddScene(Home_Scene(engine, graphic))
    engine.Run()

main()