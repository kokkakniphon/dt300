import pygame

class Image(pygame.sprite.Sprite):
    def __init__(self, image):
        pygame.sprite.Sprite.__init__(self)
        self.SetImage(image)

    def SetImage(self, image):
        self.image = image
        self.rect = self.image.get_rect()

class Button(Image):
    def __init__(self, image):
        Image.__init__(self, image)
    def Clicked(self):
        x, y = pygame.mouse.get_pos()
        if self.rect.collidepoint(x,y):
            return True
        return False

class Graphic:
    def __init__(self):
        self.font = pygame.font.Font("resources/Fonts/BalooChettan2-Bold.ttf", 50)

        self.node = pygame.image.load("resources/Assets/Node.png").convert_alpha()
        self.node_highlight = pygame.image.load("resources/Assets/Node_Highlight.png").convert_alpha()
        self.node_selected = pygame.image.load("resources/Assets/Node_Selected.png").convert_alpha()
        self.arrow = pygame.image.load("resources/Assets/Arrow.png").convert_alpha()
        self.arrow_selected = pygame.image.load("resources/Assets/Arrow_Selected.png").convert_alpha()

        self.button_start = pygame.image.load("resources/Assets/Start_Button.png").convert_alpha()
        self.button_reset = pygame.image.load("resources/Assets/Reset_Button.png").convert_alpha()
        self.button_clear = pygame.image.load("resources/Assets/Clear_Button.png").convert_alpha()
        self.button_node_selected = pygame.image.load("resources/Assets/Node_Selected_Button.png").convert_alpha()
        self.button_node_unselected = pygame.image.load("resources/Assets/Node_Unselected_Button.png").convert_alpha()
        self.button_arrow_selected = pygame.image.load("resources/Assets/Arrow_Selected_Button.png").convert_alpha()
        self.button_arrow_unselected = pygame.image.load("resources/Assets/Arrow_Unselected_Button.png").convert_alpha()
        self.button_dfs_selected = pygame.image.load("resources/Assets/DFS_Selected_Button.png").convert_alpha()
        self.button_dfs_unselected = pygame.image.load("resources/Assets/DFS_Unselected_Button.png").convert_alpha()
        self.button_dijk_selected = pygame.image.load("resources/Assets/Dijk_Selected_Button.png").convert_alpha()
        self.button_dijk_unselected = pygame.image.load("resources/Assets/Dijk_Unselected_Button.png").convert_alpha()
        self.button_kruskal_selected = pygame.image.load("resources/Assets/Kruskal_Selected_Button.png").convert_alpha()
        self.button_kruskal_unselected = pygame.image.load("resources/Assets/Kruskal_Unselected_Button.png").convert_alpha()
        
        self.panel_bg = pygame.image.load("resources/Panels/Background.png").convert_alpha()

    def SetFontSize(self, size):
        self.font = pygame.font.Font("resources/Fonts/BalooChettan2-Bold.ttf", size)