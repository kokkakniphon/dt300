import pygame
import math

from graphic import Graphic, Image
from graphic import Button
from dfs import DFSGraph
from dijkstra import DijkstraGraph
from dijkstra import DijkstraNode
from dijkstra import DijkstraPair
from kruskal import KruskalGraph

def blit_text(surface, text, pos, font, color=pygame.Color('black')):
    words = [word.split(' ') for word in text.splitlines()]  # 2D array where each row is a list of words.
    space = font.size(' ')[0]  # The width of a space.
    max_width, max_height = surface.get_size()
    x, y = pos
    for line in words:
        for word in line:
            word_surface = font.render(word, 0, color)
            word_width, word_height = word_surface.get_size()
            if x + word_width >= max_width:
                x = pos[0]  # Reset the x.
                y += word_height  # Start on new row.
            surface.blit(word_surface, (x, y))
            x += word_width + space
        x = pos[0]  # Reset the x.
        y += word_height  # Start on new row.

class NodeData:
    def __init__(self, graphic, render, index, pos):
        self.index = index
        self.pos = pos
        self.graphic = graphic
        self.render = render
        self.size = 56

        self.highlight = False

        self.node_bttn = Button(self.graphic.node)
        self.render.add(self.node_bttn);
        self.graphic.SetFontSize(30)
        self.node_text = Image(self.graphic.font.render(str(self.index), True, (255,255,255)))
        self.node_text.rect.center = self.node_bttn.rect.center
        self.render.add(self.node_text)

        self.SetPosition(self.pos)

    def __del__(self):
        self.render.remove(self.node_text)
        self.render.remove(self.node_bttn);
    
    def SetPosition(self, pos):
        self.pos = pos
        self.node_bttn.rect.center = self.pos
        self.node_text.rect.center = self.pos
    
    def IsPointerHover(self):
        x,y = pygame.mouse.get_pos()
        if (math.dist((x,y), self.pos) <= self.size/2):
            return True
        else:
            return False
    def SetHighlight(self, highlight):
        self.highlight = highlight
        if (self.highlight == True):
            self.node_bttn.image = self.graphic.node_selected
        else:
            self.node_bttn.image = self.graphic.node

class PathData:
    def __init__(self, graphic, render, source, dest):
        self.graphic = graphic
        self.render = render
        self.source = source
        self.dest = dest
        
        self.arrow_head = Image(self.graphic.arrow)
        self.render.add(self.arrow_head)

        self.target_color = (77,77,77)
        self.target_arrow_image = Image(self.graphic.arrow).image
        self.weight = 0
        self.UpdatePosition()

        self.directional = True
        
    
    def __del__(self):
        self.render.remove(self.arrow_head)

    def SetDirectional(self, val):
        self.directional = val
        if self.directional:
            self.render.add(self.arrow_head)
        else:
            self.render.remove(self.arrow_head)

    def UpdatePosition(self):
        self.dirX = (self.dest.pos[0] - self.source.pos[0])
        self.dirY = (self.dest.pos[1] - self.source.pos[1])
        self.dirXn = self.dirX / math.dist(self.source.pos, self.dest.pos)
        self.dirYn = self.dirY / math.dist(self.source.pos, self.dest.pos)
        self.weight = math.dist(self.source.pos, self.dest.pos)
        if (self.dirX != 0):
            flipX = 180
            if (self.dirX > 0):
                flipX = 0
            self.arrow_head.SetImage(pygame.transform.rotate(self.target_arrow_image, (-math.atan(self.dirY/self.dirX) * 180 / 3.1415926) + 90 + flipX))
        self.arrow_head.rect.center = ((self.source.pos[0] + self.dirX - self.dirXn * 35) , (self.source.pos[1] + self.dirY  - self.dirYn * 35))

    def Draw(self, engine):
        pygame.draw.line(engine, self.target_color, (self.source.pos[0] + self.dirXn * 30, self.source.pos[1] + self.dirYn * 30), (self.dest.pos[0] - self.dirXn * 30, self.dest.pos[1] - self.dirYn * 30), 5)
    
    def CheckHighlight(self):
        if (self.source.highlight and self.dest.highlight):
            self.target_arrow_image = Image(self.graphic.arrow_selected).image
            self.target_color = (61,139,206)
        else:
            self.target_arrow_image = Image(self.graphic.arrow).image
            self.target_color = (77,77,77)
        self.UpdatePosition()

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass
 
    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass
 
    return False

class Home_Scene:
    def __init__(self, engine, graphic):
        self.active = True
        self.engine = engine
        self.graphic = graphic
        self.sprites = pygame.sprite.Group()

        self.add_option = 0
        self.algo_option = 1
        self.node_data = []
        self.path_data = []

        self.hover_node = -1
        self.holding_node = -1

        self.source_path = -1
        self.dest_path = -1

        self.start_index = 0
        self.is_clear = True

        self.output_text = ""

    def Start(self):
        # Background
        self.background_panel = Image(self.graphic.panel_bg)
        self.background_panel.rect.center = (self.engine.WINDOW_WIDTH/2, self.engine.WINDOW_HEIGHT/2)
        self.sprites.add(self.background_panel)

        # Start Button
        self.start_bttn = Button(self.graphic.button_start)
        self.start_bttn.rect.left = 108
        self.start_bttn.rect.top = 37
        self.sprites.add(self.start_bttn)

        # Reset Button
        self.reset_bttn = Button(self.graphic.button_reset)
        self.reset_bttn.rect.left = 174
        self.reset_bttn.rect.top = 37
        self.sprites.add(self.reset_bttn)

        # Clear Button
        self.clear_bttn = Button(self.graphic.button_clear)
        self.clear_bttn.rect.left = 836
        self.clear_bttn.rect.top = 10
        self.sprites.add(self.clear_bttn)

        # Node Button
        self.node_bttn = Button(self.graphic.button_node_unselected)
        self.node_bttn.rect.left = 259
        self.node_bttn.rect.top = 60
        self.sprites.add(self.node_bttn)

        # Arrow Button
        self.arrow_bttn = Button(self.graphic.button_arrow_unselected)
        self.arrow_bttn.rect.left = 259
        self.arrow_bttn.rect.top = 93
        self.sprites.add(self.arrow_bttn)

        # DFS Button
        self.dfs_bttn = Button(self.graphic.button_dfs_unselected)
        self.dfs_bttn.rect.left = 259
        self.dfs_bttn.rect.top = 179
        self.sprites.add(self.dfs_bttn)

        # DIJK Button
        self.dijk_bttn = Button(self.graphic.button_dijk_unselected)
        self.dijk_bttn.rect.left = 259
        self.dijk_bttn.rect.top = 212
        self.sprites.add(self.dijk_bttn)

        # KRUSKAL Button
        self.kruskal_bttn = Button(self.graphic.button_kruskal_unselected)
        self.kruskal_bttn.rect.left = 259
        self.kruskal_bttn.rect.top = 245
        self.sprites.add(self.kruskal_bttn)
        
        self.SetAlgoOption(1)

        # Start Index Text
        self.start_index_text = Image(self.graphic.font.render(str(self.start_index), True, (77,77,77)))
        self.start_index_text.rect.center = (60, 65)
        self.sprites.add(self.start_index_text)

        # Console Text
        self.console_text = Image(self.graphic.font.render("", True, (255,255,255)))
        self.console_text.rect.center = (125, 305)
        pass


    def OnEnable(self):
        pass
    def OnDisable(self):
        pass
    def Update(self):
        if not self.active:
            return

        self.sprites.update()
        for event in self.engine.event:

            if event.type == pygame.MOUSEBUTTONUP:
                if self.is_clear:
                    if event.button == 1:
                        self.holding_node = -1

                        if (self.add_option == 2):
                            if (self.source_path != -1):
                                if (self.hover_node != self.source_path and self.hover_node != -1):
                                    self.dest_path = self.hover_node
                                    self.path_data.append(PathData(self.graphic, self.sprites, self.node_data[self.source_path], self.node_data[self.dest_path]))
                                self.source_path = -1
                                self.dest_path = -1
                    
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    if (self.start_bttn.Clicked()):
                        self.StartButton()
                    if (self.reset_bttn.Clicked()):
                        self.ResetButton()
                    if (self.clear_bttn.Clicked()):
                        self.ClearButton()
                        continue

                    if (self.node_bttn.Clicked()):
                        self.SetAddOption(1)
                    if (self.arrow_bttn.Clicked()):
                        self.SetAddOption(2)

                    if (self.dfs_bttn.Clicked()):
                        self.SetAlgoOption(1)
                    if (self.dijk_bttn.Clicked()):
                        self.SetAlgoOption(2)
                    if (self.kruskal_bttn.Clicked()):
                        self.SetAlgoOption(3)

                    if self.is_clear:
                        x,y = pygame.mouse.get_pos()
                        if (self.add_option == 1):
                            if (x > 350 and self.hover_node == -1):
                                self.node_data.append(NodeData(self.graphic, self.sprites, len(self.node_data), (x,y)))
                                self.hover_node = len(self.node_data) - 1
                                self.node_data[self.hover_node].node_bttn.image = self.graphic.node_highlight
                            if (self.hover_node != -1 and x > 350):
                                self.holding_node = self.hover_node

                        if (self.add_option == 2):
                            if (self.hover_node != -1 and x > 350):
                                self.source_path = self.hover_node
                pass
            if event.type == pygame.MOUSEMOTION:
                if self.is_clear:
                    if (self.holding_node != -1):
                        x,y = pygame.mouse.get_pos()
                        self.node_data[self.holding_node].SetPosition((x,y))
                        for path in self.path_data:
                            path.UpdatePosition()

                    isHovered = False
                    for i in range(len(self.node_data)):
                        if (self.node_data[i].IsPointerHover()):
                            if (self.hover_node != -1):
                                self.node_data[self.hover_node].node_bttn.image = self.graphic.node
                            self.hover_node = i;
                            self.node_data[i].node_bttn.image = self.graphic.node_highlight
                            isHovered = True
                            break
                    if (isHovered == False):
                        if (self.hover_node != -1):
                            self.node_data[self.hover_node].node_bttn.image = self.graphic.node
                        self.hover_node = -1
            if event.type == pygame.KEYDOWN:
                if is_number(event.unicode):
                    self.start_index = int(event.unicode)
                    self.graphic.SetFontSize(50)
                    self.start_index_text.SetImage(self.graphic.font.render(str(self.start_index), True, (77,77,77)))
                    self.start_index_text.rect.center = (60, 65)

        pass
    def Render(self):
        if not self.active:
            return
        self.sprites.draw(self.engine._window)
        for path in self.path_data:
            path.Draw(self.engine._window)
        blit_text(self.engine._window, self.output_text, (21, 119), self.graphic.font, (255,255,255))
        pass

    def StartButton(self):
        self.graphic.SetFontSize(30)
        self.is_clear = False
        if (self.algo_option == 1):
            dfsG = DFSGraph()
            for path in self.path_data:
                dfsG.addEdge(path.source.index, path.dest.index)

            result = dfsG.DFS(self.start_index)

            self.output_text = "DFS:\n"

            for d in result:
                self.output_text += str(d) + "\n"
                self.node_data[d].SetHighlight(True)
            for path in self.path_data:
                path.CheckHighlight()

        if (self.algo_option == 2):
            dijkG = DijkstraGraph()
            v = []
            n = len(self.node_data)
            s = self.start_index

            for i in range(n):
                a = DijkstraNode(i)
                v.append(a)
            
            for path in self.path_data:
                v[path.source.index].Add_child(path.dest.index, int(path.weight))
            
            path = [0 for i in range(len(v))]
            dist = dijkG.dijkstraDist(v, s, path)

            self.output_text = "Dijkstra:\n"

            for i in range(len(dist)):
                if (dist[i] == 1000000000):
                    self.node_data[i].SetHighlight(False)
                    continue
                self.node_data[i].SetHighlight(True)
                self.output_text += "{} -> {} == {}\n".format(s, i, dist[i])
            for path in self.path_data:
                path.CheckHighlight()
            
            self.console_text.SetImage(self.graphic.font.render(self.output_text, True, (255,255,255)))
            self.console_text.rect.center = (125, 305)
        
        if (self.algo_option == 3):
            kruskalG = KruskalGraph(len(self.node_data))
            for path in self.path_data:
                kruskalG.addEdge(path.source.index, path.dest.index, int(path.weight))

            result = kruskalG.KruskalMST()
            self.output_text = kruskalG.GetPrint(result)
            for u, v, weight in result:
                self.node_data[u].SetHighlight(True)
                self.node_data[v].SetHighlight(True)
            for path in self.path_data:
                path.CheckHighlight()
            pass
    def ResetButton(self):
        for node in self.node_data:
            node.SetHighlight(False)
        for path in self.path_data:
            path.CheckHighlight()
        self.output_text = ""
        self.is_clear = True

    def ClearButton(self):
        self.node_data.clear()
        self.path_data.clear()
        self.is_clear = True

    def SetAddOption(self, index):
        if (self.add_option == index):
            self.add_option = 0
        else:
            self.add_option = index

        self.node_bttn.image = self.graphic.button_node_unselected
        self.arrow_bttn.image = self.graphic.button_arrow_unselected
        if (self.add_option == 1):
            self.node_bttn.image = self.graphic.button_node_selected
        elif (self.add_option == 2):
            self.arrow_bttn.image = self.graphic.button_arrow_selected

    def SetAlgoOption(self, index):
        self.algo_option = index

        self.dfs_bttn.image = self.graphic.button_dfs_unselected
        self.dijk_bttn.image = self.graphic.button_dijk_unselected
        self.kruskal_bttn.image = self.graphic.button_kruskal_unselected
        if (self.algo_option == 1):
            self.dfs_bttn.image = self.graphic.button_dfs_selected
            for path in self.path_data:
                path.SetDirectional(True)
        elif (self.algo_option == 2):
            self.dijk_bttn.image = self.graphic.button_dijk_selected
            for path in self.path_data:
                path.SetDirectional(True)
        elif (self.algo_option == 3):
            self.kruskal_bttn.image = self.graphic.button_kruskal_selected
            for path in self.path_data:
                path.SetDirectional(False)