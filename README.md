# README #

### What is this repository for? ###

* This repository is for showcasing the searching algorithm (DFS, Dijkstra, and Kruskal)

### How do I get set up? ###

* Clone/Download the project
* Run the "DT300_DFS-DIJK-KRUSKAL-Niphon.exe" file

** Make sure that "resource" folder is on the same root as the executable file **

### How to use ###


* After open the application you will see this screen

![Main Screen](https://bitbucket.org/kokkakniphon/dt300/raw/master/readme_image/MainScene.png)


* Click on node, and LEFT-CLICK on white space to place new node, you can drag it around by holding it

![Step 1](https://bitbucket.org/kokkakniphon/dt300/raw/master/readme_image/Step1.png)


* Click on arrow, and drag from one node to another and release to create directional link between node.

![Step 2](https://bitbucket.org/kokkakniphon/dt300/raw/master/readme_image/Step2.png)


* You can set the target algorithm under ALGO slot, then press START to run the algorithm

![Step 3](https://bitbucket.org/kokkakniphon/dt300/raw/master/readme_image/Step3.png)


* You will have to click RESET button to edit the current structure

![Step 4](https://bitbucket.org/kokkakniphon/dt300/raw/master/readme_image/Step4.png)


* After some edit, now let try Dijkstra' algorithm

![Step 5](https://bitbucket.org/kokkakniphon/dt300/raw/master/readme_image/Step5.png)


* Click CLEAR to delete all node and arrow

![Step 6](https://bitbucket.org/kokkakniphon/dt300/raw/master/readme_image/Step6.png)


* You can also set the start node by typing number 0-9 on your keyboard

![Step 7](https://bitbucket.org/kokkakniphon/dt300/raw/master/readme_image/Step7.png)