import sys
 
infi = 1000000000;

class DijkstraPair:
    def __init__(self, first, second):
        self.first = first
        self.second = second

class DijkstraNode:
    def __init__(self, vertexNumber):       
        self.vertexNumber = vertexNumber
        self.children = []

    def Add_child(self, vNumber, length):   
        p = DijkstraPair(vNumber, length);
        self.children.append(p);

class DijkstraGraph():
    def dijkstraDist(self, g, s, path):
        
        dist = [infi for i in range(len(g))]

        visited = [False for i in range(len(g))]
        
        for i in range(len(g)):       
            path[i] = -1
        dist[s] = 0;
        path[s] = -1;
        current = s;
    
        sett = set()    
        while (True):
            
            visited[current] = True;
            for i in range(len(g[current].children)): 
                v = g[current].children[i].first;           
                if (visited[v]):
                    continue;
                sett.add(v);
                alt = dist[current] + g[current].children[i].second;
    
                if (alt < dist[v]):      
                    dist[v] = alt;
                    path[v] = current;       
            if current in sett:           
                sett.remove(current);       
            if (len(sett) == 0):
                break;
    
            minDist = infi;
            index = 0;
            for a in sett:       
                if (dist[a] < minDist):          
                    minDist = dist[a];
                    index = a;          
            current = index;  
        return dist;
    def printPath(self, path, i, s):
        if (i != s):
            if (path[i] == -1):       
                print("Path not found!!");
                return;       
            self.printPath(path, path[i], s);
            print(path[i] + " ");